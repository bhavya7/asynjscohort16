/**
 * read all the files in data folder and write them syncronously in a text file
 */

const fs = require("fs");
const path = require("path");

const folder = path.join(__dirname, "../data");
console.log(folder);

// Get the names of all the files in the directory:
fs.readdir(folder, (err, data) => {
  const outFile = "outText.txt";
  const outFilePath = path.join(__dirname, outFile);
  if (err) console.log(err);
  if (data) {
    data.forEach((fileName) => {
      const filePath = path.join(folder, fileName);
      fs.readFile(filePath, "utf-8", (err, fileData) => {
        if (err) console.log(err);
        if (fileData) {
          //Check if the outFile exists
          fs.access(outFilePath, fs.constants.F_OK, (err) => {
            if (err) {
              fs.writeFile(outFilePath, fileData, (err) => {
                if (err) console.log(err);
              });
            } else {
              console.log("File Exists");
              fs.appendFile(outFilePath, fileData, (err) => {
                if (err) console.log(err);
              });
            }
          });
        }
      });
    });
  }
});
